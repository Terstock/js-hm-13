/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript? - цей метод каже браузеру, що ми не хочемо аби відбувалась ця дія. Зазвичай це є базовою дією елемента браузеру.
2. В чому сенс прийому делегування подій? - в тому аби створити обробник відразу для всіх елементів, а не створювати для кожного свій
3. Які ви знаєте основні події документу та вікна браузера? DOMContentLoaded,  load, before unload, unload

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для 
потрібної вкладки. При цьому решта тексту повинна бути прихована.
У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

let tabs = document.querySelector(".tabs");
let titles = tabs.querySelectorAll("li");
let tabsContent = document.querySelector(".tabs-content");
let lis = tabsContent.querySelectorAll("li");

tabs.addEventListener("click", function (event) {
  lis.forEach((li) => {
    li.classList.remove("active");
  });

  titles.forEach((title) => {
    title.classList.remove("active");
  });

  let activeTab = event.target.closest(`.tabs-title`);
  activeTab.getAttribute("data-name");
  activeTab.classList.add("active");

  for (let i = 0; i < lis.length; i++) {
    if (activeTab.dataset.name === lis[i].dataset.name) {
      lis[i].classList.add("active");
    }
  }
});
